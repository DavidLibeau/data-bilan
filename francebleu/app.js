var fs = require('fs-extra');

async function main() {
  var count = {};
  var files = await fs.readdir("2021");
  for (var f in files) {
    if (files[f].slice(0, 1) != ".") {
      var data = await fs.readJson("2021/" + files[f], 'utf8');
      count[files[f].replace(".json","")+"/01/21"] = data.length;
    }
  }
  var files = await fs.readdir("2022");
  for (var f in files) {
    if (files[f].slice(0, 1) != ".") {
      var data = await fs.readJson("2022/" + files[f], 'utf8');
      count[files[f].replace(".json","")+"/01/22"] = data.length;
    }
  }

  for (var c in count) {
    console.log(c+";"+count[c]);
  }
}
main();