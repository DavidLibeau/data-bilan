const parse = require('csv-parse');
const fs = require('fs-extra');

const processFile = async (path) => {
    records = []
    const parser = fs
        .createReadStream(path)
        .pipe(parse({
            // CSV options if any
            delimiter: ";"
        }));
    for await (const record of parser) {
        records.push(record)
    }
    return records
}

var pnData = {};
var gnData = {};
var totalStats = {};
var categorieStats = {};
var fraudeFiscaleStats = {};

(async () => {
    var files = await fs.readdir("data/pn");
    for (var f in files) {
        var annee = files[f].split("-")[files[f].split("-").length - 1].slice(-8, -4);
        console.log(annee);
        pnData[annee] = await processFile("data/pn/" + files[f])
    }
    var files = await fs.readdir("data/gn");
    for (var f in files) {
        var annee = files[f].split("-")[files[f].split("-").length - 1].slice(-8, -4);
        console.log(annee);
        gnData[annee] = await processFile("data/gn/" + files[f])
    }

    for (var annee in gnData) {
        manageData(pnData[annee], annee);
    }
    for (var annee in gnData) {
        manageData(gnData[annee], annee);
    }
    
    console.log(totalStats);
    console.log("fraudeFiscaleStats", fraudeFiscaleStats);
    console.log(categorieStats);
    for(var annee in categorieStats){
        console.log(annee+", "+categorieStats[annee]["Règlements de compte entre malfaireurs"]+", "+categorieStats[annee]["Vols"]+", "+categorieStats[annee]["Violences sexuelles"]+", "+categorieStats[annee]["Stupéfiants"]+", "+categorieStats[annee]["Biens"]+", "+categorieStats[annee]["Atteintes aux intérêts fondamentaux de la Nation"]+", "+categorieStats[annee]["Faux & escroqueries"]+", "+categorieStats[annee]["Entreprises"]);
    }
})()

function manageData(data, annee) {
    var total = 0;
    var totalCategorie = {};
    var totalFraudeFiscale = 0;
    for (var i = 3; i < data.length; i++) {
        if (replaceCategorie[data[i][1]]) {
            totalCategorie[replaceCategorie[data[i][1]]] = 0;
        }
        for (var j = 2; j < data[i].length; j++) {
            var value = Number.parseInt(data[i][j], 10);
            if (value && typeof value === "number") {
                if (replaceCategorie[data[i][1]]) {
                    if (!totalCategorie[replaceCategorie[data[i][1]]]) {
                        totalCategorie[replaceCategorie[data[i][1]]] = 0;
                    }
                    totalCategorie[replaceCategorie[data[i][1]]] += value;
                }
                if(data[i][1]=="Fraudes fiscales"){
                    totalFraudeFiscale += value;
                }
                total += value;
            }
        }
    }
    if (!totalStats[annee]) {
        totalStats[annee] = 0;
    }
    totalStats[annee] += total;
    
    if (!fraudeFiscaleStats[annee]) {
        fraudeFiscaleStats[annee] = 0;
    }
    fraudeFiscaleStats[annee] += totalFraudeFiscale;

    if(data)
    if (!categorieStats[annee]) {
        categorieStats[annee] = totalCategorie;
    } else {
        for (var c in totalCategorie) {
            if (categorieStats[annee][c]) {
                categorieStats[annee][c] = 0;
            }
            categorieStats[annee][c] += totalCategorie[c];
        }
    }
}


var replaceCategorie = {
    "Règlements de compte entre malfaireurs": "Règlements de compte entre malfaireurs",
    "Homicides pour voler et à l'occasion de vols": "Vols",
    "Tentatives d'homicides pour voler et à l'occasion de vols": "Vols",
    "Prises d'otages à l'occasion de vols": "Vols",
    "Violations de domicile": "Vols",
    "Vols à main armée contre des établissements financiers": "Vols",
    "Vols à main armée contre des éts industriels ou commerciaux": "Vols",
    "Vols à main armée contre des entreprises de transports de fonds": "Vols",
    "Vols à main armée contre des particuliers à leur domicile": "Vols",
    "Autres vols à main armée": "Vols",
    "Vols avec armes blanches contre des établissements financiers,commerciaux ou industriels": "Vols",
    "Vols avec armes blanches contre des particuliers à leur domicile": "Vols",
    "Autres vols avec armes blanches": "Vols",
    "Vols violents sans arme contre des établissements financiers,commerciaux ou industriels": "Vols",
    "Vols violents sans arme contre des particuliers à leur domicile": "Vols",
    "Vols violents sans arme contre des femmes sur voie publique ou autre lieu public": "Vols",
    "Vols violents sans arme contre d'autres victimes": "Vols",
    "Cambriolages de locaux d'habitations principales": "Vols",
    "Cambriolages de résidences secondaires": "Vols",
    "Camb.de  locaux industriels, commerciaux ou financiers": "Vols",
    "Cambriolages d'autres lieux": "Vols",
    "Vols avec entrée par ruse en tous lieux": "Vols",
    "Vols à la tire": "Vols",
    "Vols à l'étalage": "Vols",
    "Vols de véhicules de transport avec frêt": "Vols",
    "Vols d'automobiles": "Vols",
    "Vols de véhicules motorisés à 2 roues": "Vols",
    "Vols à la roulotte": "Vols",
    "Vols d''accessoires sur véhicules à moteur immatriculés": "Vols",
    "Vols simples sur chantier": "Vols",
    "Vols simples sur exploitations agricoles": "Vols",
    "Autres vols simples contre des établissements publics ou privés": "Vols",
    "Autres vols simples contre des particuliers dans  deslocaux privés": "Vols",
    "Autres vols simples contre des particuliers dans des locaux ou lieux publics": "Vols",
    "Recels": "Vols",
    "Proxénétisme": "Violences sexuelles",
    "Viols sur des majeur(e)s": "Violences sexuelles",
    "Viols sur des mineur(e)s": "Violences sexuelles",
    "Harcèlements sexuels et autres agressions sexuelles contre des majeur(e)s": "Violences sexuelles",
    "Harcèlements sexuels et autres agressions sexuelles contre des mineur(e)s": "Violences sexuelles",
    "Atteintes sexuelles": "Violences sexuelles",
    "Trafic et revente sans usage de stupéfiants": "Stupéfiants",
    "Usage-revente de stupéfiants": "Stupéfiants",
    "Usage de stupéfiants": "Stupéfiants",
    "Autres infractions à la législation sur les stupéfiants": "Stupéfiants",
    "Incendies volontaires de biens publics": "Biens",
    "Incendies volontaires de biens privés": "Biens",
    "Attentats à l'explosif contre des biens publics": "Biens",
    "Attentats à l'explosif contre des biens privés": "Biens",
    "Autres destructions er dégradations de biens publics": "Biens",
    "Autres destructions er dégradations de biens privés": "Biens",
    "Destructions et dégradations de véhicules privés": "Biens",
    "Atteintes aux intérêts fondamentaux de la Nation": "Atteintes aux intérêts fondamentaux de la Nation",
    "Faux documents d'identité": "Faux & escroqueries",
    "Faux documents concernant la circulation des véhicules": "Faux & escroqueries",
    "Autres faux documents administratifs": "Faux & escroqueries",
    "Faux en écriture publique et authentique": "Faux & escroqueries",
    "Autres faux en écriture": "Faux & escroqueries",
    "Fausse monnaie": "Faux & escroqueries",
    "Contrefaçons et fraudes industrielles et commerciales": "Faux & escroqueries",
    "Contrefaçons littéraires et artistique": "Faux & escroqueries",
    "Falsification et usages de chèques volés": "Faux & escroqueries",
    "Falsification et usages de cartes de crédit": "Faux & escroqueries",
    "Escroqueries et abus de confiance": "Faux & escroqueries",
    "Infractions à la législation sur les chèques": "Faux & escroqueries",
    "Prix illicittes, publicité fausse et infractions aux règles de la concurrence": "Entreprises",
    "Achats et ventes sans factures": "Entreprises",
    "Infractions à l'exercice d'une profession règlementée": "Entreprises",
    "Infractions au droit de l'urbanisme et de la construction": "Entreprises",
    "Fraudes fiscales": "Entreprises",
    "Autres délits économiques et financiers": "Entreprises"
};