fs = require('fs-extra');

var stats = {
  'PO800538': {
    'libelle': 'Renaissance',
    'nombreMembresGroupe': 172,
    'votes': {
      'nonVotants': 0,
      'pour': 0,
      'contre': 0,
      'abstentions': 0,
      'nonVotantsVolontaires': 0
    }
  },
  'PO800520': {
    'libelle': 'RN',
    nombreMembresGroupe: 89,
    'votes': {
      'nonVotants': 0,
      'pour': 0,
      'contre': 0,
      'abstentions': 0,
      'nonVotantsVolontaires': 0
    }
  },
  'PO800490': {
    'libelle': 'LFI',
    nombreMembresGroupe: 75,
    'votes': {
      'nonVotants': 0,
      'pour': 0,
      'contre': 0,
      'abstentions': 0,
      'nonVotantsVolontaires': 0
    }
  },
  'PO800508': {
    'libelle': 'LR',
    nombreMembresGroupe: 62,
    'votes': {
      'nonVotants': 0,
      'pour': 0,
      'contre': 0,
      'abstentions': 0,
      'nonVotantsVolontaires': 0
    }
  },
  'PO800484': {
    'libelle': 'MoDem',
    nombreMembresGroupe: 48,
    'votes': {
      'nonVotants': 0,
      'pour': 0,
      'contre': 0,
      'abstentions': 0,
      'nonVotantsVolontaires': 0
    }
  },
  'PO800496': {
    'libelle': 'SOC',
    nombreMembresGroupe: 31,
    'votes': {
      'nonVotants': 0,
      'pour': 0,
      'contre': 0,
      'abstentions': 0,
      'nonVotantsVolontaires': 0
    }
  },
  'PO800514': {
    'libelle': 'Horizons',
    nombreMembresGroupe: 30,
    'votes': {
      'nonVotants': 0,
      'pour': 0,
      'contre': 0,
      'abstentions': 0,
      'nonVotantsVolontaires': 0
    }
  },
  'PO800526': {
    'libelle': 'ECOLO',
    nombreMembresGroupe: 23,
    'votes': {
      'nonVotants': 0,
      'pour': 0,
      'contre': 0,
      'abstentions': 0,
      'nonVotantsVolontaires': 0
    }
  },
  'PO800502': {
    'libelle': 'GDR',
    nombreMembresGroupe: 22,
    'votes': {
      'nonVotants': 0,
      'pour': 0,
      'contre': 0,
      'abstentions': 0,
      'nonVotantsVolontaires': 0
    }
  },
  'PO800532': {
    'libelle': 'LIOT',
    nombreMembresGroupe: 16,
    'votes': {
      'nonVotants': 0,
      'pour': 0,
      'contre': 0,
      'abstentions': 0,
      'nonVotantsVolontaires': 0
    }
  },
  'PO793087': {
    'libelle': 'NI',
    nombreMembresGroupe: 9,
    'votes': {
      'nonVotants': 0,
      'pour': 0,
      'contre': 0,
      'abstentions': 0,
      'nonVotantsVolontaires': 0
    }
  }
};


async function main() {
  var files = await fs.readdir("data");
  for (var f in files) {
    if (files[f].slice(0, 1) != ".") {
      var data = await fs.readJson("data/" + files[f], 'utf8');
      manageScrutin(data);
    }
  }

  console.log(stats);
  for(var s in stats){
    var c = stats[s]['libelle']+',';
    for(var v in stats[s]['votes']){
      c+=stats[s]['votes'][v]+',';
    }
    c+=stats[s]['nombreMembresGroupe'];
    console.log(c);
  }
}
main();

function manageScrutin(data) {
  if (!data) {
    console.log(data);
  } else {
    var votesGroupes = data["scrutin"]["ventilationVotes"]["organe"]["groupes"]["groupe"];
    for (var v in votesGroupes) {
      if (stats[votesGroupes[v]["organeRef"]]) {
        for (var d in votesGroupes[v]["vote"]["decompteVoix"]) {
          stats[votesGroupes[v]["organeRef"]]["votes"][d] += parseInt(votesGroupes[v]["vote"]["decompteVoix"][d]);
        }
      } else {
        console.log("Error groupe not found " + votesGroupes[v]["organeRef"]);
      }
    }
  }
}
