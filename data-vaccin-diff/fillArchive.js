const fs = require('fs');
const {
    parse
} = require('csv-parse');
const stringifyCSV = require('csv-stringify')
const fetch = (...args) => import('node-fetch').then(({
    default: fetch
}) => fetch(...args));
const moment = require('moment');
const jsdom = require("jsdom");
const {
    JSDOM
} = jsdom;

async function main() {
    var rootUrl = "http://files.opendatarchives.fr/data.gouv.fr/sante-publique-france/archives/donnees-relatives-aux-personnes-vaccinees-contre-la-covid-19-1/";
    var response = await fetch(rootUrl);
    var body = await response.text();
    const dom = new JSDOM(body);
    var aElements = dom.window.document.querySelectorAll("a");
    for (var a in aElements) {
        if (aElements[a]?.href && aElements[a].href.search("vacsi-fra-") != -1) {
            var fileName = aElements[a].href;
            console.log(fileName);
            try {
                fs.statSync('archive/' + fileName);
            } catch (e) {
                var file = await fetch(rootUrl + fileName);
                var data = await file.text();
                data = data.replaceAll(",", ";");
                fs.writeFile("archive/" + fileName, data, (err) => {
                    if (err)
                        console.log(err);
                });
            }

        }
    }
}
main();