const fs = require('fs');
const {
    parse
} = require('csv-parse');
const stringifyCSV = require('csv-stringify')
const fetch = (...args) => import('node-fetch').then(({
    default: fetch
}) => fetch(...args));
const moment = require('moment');
const jsdom = require("jsdom");
const {
    JSDOM
} = jsdom;

var notModifiedData = {};

async function parseFile(filepath) {
    records = []
    const parser = fs
        .createReadStream(filepath)
        .pipe(parse({
            delimiter: ";",
            columns: true,
            skip_empty_lines: true
        }));
    for await (const record of parser) {
        records[record['jour']] = record;
    }
    return records
}

async function generateNotModified(currentData, notModifiedData) {
    var cols = ["n_dose1", "n_complet", "n_rappel"];
    var result = {};
    var days = Object.keys(currentData).slice(0, -10);
    for (var c in cols) {
        var col = cols[c];
        for (var d in days) {
            var day = days[d];
            if (!notModifiedData[day]) {
                notModifiedData[day] = {};
            }
            if (!notModifiedData[day][col]) {
                notModifiedData[day][col] = currentData[day][col];
            }
        }
    }
    return;
}

async function main() {
    fs.readdir(__dirname + "/archive", async (err, files) => {
        if (err)
            console.log(err);
        else {
            var previousData = {};
            for (var f in files) {
                var file = files[f];
                console.log(file);
                var currentData = await parseFile('./archive/' + file);
                await generateNotModified(currentData, notModifiedData);
            }
            console.log("---");
            var csvData = "date;n_dose1_notModified;n_complet_notModified;n_rappel_notModified";
            for (var d in notModifiedData) {
                csvData += "\n" + d + ";" + (notModifiedData[d]["n_dose1"] ?? 0) + ";" + (notModifiedData[d]["n_complet"] ?? 0) + ";" + (notModifiedData[d]["n_rappel"] ?? 0);
            }
            fs.writeFile("vaccin-notModified.csv", csvData, (err) => {
                if (err)
                    console.log(err);
            });
        }
    })
}
main();