# Data vaccin diff

Scripts that analyze diff into vaccin's open data

## Scripts

- `fillArchive.js`: get the last data from opendatarchive.
- `calculateDiff.js`: display all the diff (without 10 recent days).
- `calculateDiffDaysBefore.js`: analyze days with modifications.
- `generateNotModified.js`: create a dataset with data not modified.