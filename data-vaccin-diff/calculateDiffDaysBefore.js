const fs = require('fs');
const {
    parse
} = require('csv-parse');
const stringifyCSV = require('csv-stringify')
const fetch = (...args) => import('node-fetch').then(({
    default: fetch
}) => fetch(...args));
const moment = require('moment');
const jsdom = require("jsdom");
const {
    JSDOM
} = jsdom;

var diffData = {};
var cols = ["n_dose1", "n_complet", "n_rappel"];

async function parseFile(filepath) {
    records = []
    const parser = fs
        .createReadStream(filepath)
        .pipe(parse({
            delimiter: ";",
            columns: true,
            skip_empty_lines: true
        }));
    for await (const record of parser) {
        records[record['jour']] = record;
    }
    return records
}

async function calculateDiff(currentData, previousData) {
    var result = {};
    var days = Object.keys(currentData);
    for (var c in cols) {
        var col = cols[c];
        result[col] = {};
        for (var d in days) {
            var add = 0;
            var del = 0;
            var day = days[d];
            try {
                var diff = currentData[day][col] - previousData[day][col];
                //console.log("previous:"+previousData[day][col]);
                //console.log("current:"+previousData[day][col]);
                if (diff > 0) {
                    add += diff;
                } else if (diff < 0) {
                    del += diff;
                }
            } catch (e) {
                console.error(e.message);
            }
            result[col][days.length - d] = {
                add: add,
                del: del
            };
        }
    }
    console.log(result);
    return result;
}

async function main() {
    fs.readdir(__dirname + "/archive", async (err, files) => {
        if (err)
            console.log(err);
        else {
            var previousData = {};
            for (var f in files) {
                var file = files[f];
                console.log(file);
                var currentData = await parseFile('./archive/' + file);
                if (Object.keys(previousData).length > 0) {
                    diffData[file] = await calculateDiff(currentData, previousData);
                }
                previousData = currentData;
            }


            console.log("---");
            fs.writeFile("vaccin-diff-daysBefore.json", JSON.stringify(diffData), (err) => {
                if (err)
                    console.log(err);
            });

            for (var c in cols) {
                var col = cols[c];
                var csvData = "date;"
                for (var d in diffData) {
                    var date = d.split("vacsi-fra-")[1].split("-");
                    date = date[0] + "-" + date[1] + "-" + date[2];
                    csvData += "\n" + date;
                    for (var n in diffData[d][col]) {
                        csvData += ";" + diffData[d][col][n]["add"];
                    }
                }
                fs.writeFile("vaccin-diff-daysBefore-" + col + "-add.csv", csvData, (err) => {
                    if (err)
                        console.log(err);
                });

                var csvData = "date;"
                for (var d in diffData) {
                    var date = d.split("vacsi-fra-")[1].split("-");
                    date = date[0] + "-" + date[1] + "-" + date[2];
                    csvData += "\n" + date;
                    for (var n in diffData[d][col]) {
                        csvData += ";" + diffData[d][col][n]["del"];
                    }
                }
                fs.writeFile("vaccin-diff-daysBefore-" + col + "-del.csv", csvData, (err) => {
                    if (err)
                        console.log(err);
                });
            }
        }
    });
}
main();