const fs = require('fs');
const {
    parse
} = require('csv-parse');
const stringifyCSV = require('csv-stringify')
const fetch = (...args) => import('node-fetch').then(({
    default: fetch
}) => fetch(...args));
const moment = require('moment');
const jsdom = require("jsdom");
const {
    JSDOM
} = jsdom;

var diffData = {};

async function parseFile(filepath) {
    var records = []
    const parser = fs
        .createReadStream(filepath)
        .pipe(parse({
            delimiter: ";",
            columns: true,
            skip_empty_lines: true
        }));
    for await (const record of parser) {
        records[record['jour']] = record;
    }
    return records
}

async function calculateDiff(currentData, previousData) {
    var cols = ["n_dose1", "n_complet", "n_rappel"];
    var result = {};
    var days = Object.keys(currentData).slice(0, -10);
    for (var c in cols) {
        var col = cols[c];
        var add = 0;
        var del = 0;
        for (var d in days) {
            var day = days[d];
            try {
                var diff = currentData[day][col] - previousData[day][col];
                //console.log("previous:"+previousData[day][col]);
                //console.log("current:"+previousData[day][col]);
                if (diff > 0) {
                    add += diff;
                } else if (diff < 0) {
                    del += diff;
                }
            } catch (e) {
                console.error(e.message);
            }
        }
        result[col] = {
            add: add,
            del: del
        };
    }
    console.log(result);
    return result;
}

async function main() {
    fs.readdir(__dirname + "/archive", async (err, files) => {
        if (err)
            console.log(err);
        else {
            var previousData = {};
            for (var f in files) {
                var file = files[f];
                console.log(file);
                var currentData = await parseFile('./archive/' + file);
                if (Object.keys(previousData).length > 0) {
                    diffData[file] = await calculateDiff(currentData, previousData);
                }
                previousData = currentData;
            }
            var onlineDataset = await fetch("https://www.data.gouv.fr/api/1/datasets/donnees-relatives-aux-personnes-vaccinees-contre-la-covid-19-1/");
            onlineDataset = JSON.parse(await onlineDataset.text());
            for (var r in onlineDataset["resources"]) {
                if (onlineDataset["resources"][r]["title"].search("vacsi-fra-") != -1) {
                    console.log("online : " + onlineDataset["resources"][r]["title"]);
                    if (Object.keys(previousData).length > 0) {
                        var onlineData = await fetch(onlineDataset["resources"][r]["url"]);
                        onlineData = parse(await onlineData.text(), {
                            delimiter: ";",
                            columns: true,
                            skip_empty_lines: true
                        });
                        var currentData = []
                        for await (const record of onlineData) {
                            currentData[record['jour']] = record;
                        }
                        diffData[onlineDataset["resources"][r]["title"]] = await calculateDiff(currentData, previousData);
                    }
                }
            }
            console.log("---");
            var csvData = "date;n_dose1_add;n_complet_add;n_rappel_add;n_dose1_del;n_complet_del;n_rappel_del"
            for (var d in diffData) {
                var date = d.split("vacsi-fra-")[1].split("-");
                date = date[0] + "-" + date[1] + "-" + date[2];
                csvData += "\n" + date + ";" + diffData[d]["n_dose1"]["add"] + ";" + diffData[d]["n_complet"]["add"] + ";" + diffData[d]["n_rappel"]["add"] + ";" + diffData[d]["n_dose1"]["del"] + ";" + diffData[d]["n_complet"]["del"] + ";" + diffData[d]["n_rappel"]["del"];
            }
            fs.writeFile("vaccin-diff.csv", csvData, (err) => {
                if (err)
                    console.log(err);
            });
        }
    });

}
main();