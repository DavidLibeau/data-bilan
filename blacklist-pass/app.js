const fs = require('fs').promises;
const simpleGit = require('simple-git');

var dateVerbose = []; //["2022-02-18T22:45:14+00:00"];

async function main() {
    const git = simpleGit('TAC-Files');
    try {
        //await git.clone('https://github.com/rgrunbla/TAC-Files.git');
        await git.pull();
        var count = {};
        var filename = "blacklist_qrcode_tacv.json";
        var history = await git.log({
            file: filename
        });
        history = history["all"].reverse();
        for (var h in history) {
            console.log(history[h]["date"] + ":" + filename);
            console.log(history[h]["hash"] + ":" + filename);
            try {
                var data = await fs.readFile("cache/" + history[h]["hash"] + ":" + filename);
                console.log("↪️  from cache");
                data = JSON.parse(data);
            } catch (e) {
                console.log(e);
                var data = await git.show([history[h]["hash"] + ":" + filename]);
                console.log("↪️  from git");
                fs.writeFile("cache/" + history[h]["hash"] + ":" + filename, data, (err) => {
                    if (err)
                        console.log(err);
                });
                data = JSON.parse(data);
            }
            if (data) {
                if (data["lastIndexBlacklist"] != 0) {
                    if (!!data["lastIndexBlacklist"]) {
                        count[history[h]["date"]] = analyzeData(removeDuplicate(transformToSimpleArr(data["elements"])), dateVerbose.includes(history[h]["date"]), data["lastIndexBlacklist"]);
                    } else {
                        count[history[h]["date"]] = analyzeData(removeDuplicate(data), dateVerbose.includes(history[h]["date"]));
                    }
                }
            }
        }
        var csvData = "date;count"
        for (var d in count) {
            var date = d.split("T")[0]+" "+d.split("T")[1].split("+")[0];
            csvData += "\n" + date + ";" + count[d].total;
        }
        fs.writeFile("count.csv", csvData, (err) => {
            if (err)
                console.log(err);
        });
        logCsv(count);
    } catch (e) {
        console.log(e);
    }
}
main();

function removeDuplicate(arr) {
    var assocArr = {};
    for (var a in arr) {
        assocArr[arr[a]] = "1";
    }
    return Object.keys(assocArr);
}

function logCsv(data) {
    for (var d in data) {
        console.log(d + "," + data[d].total + "," + data[d].addition + "," + data[d].deletion);
    }
}

function transformToSimpleArr(data) {
    var transformed = [];
    for (var d in data) {
        transformed.push(data[d]["hash"]);
    }
    return transformed;
}

var previousData = [];

function analyzeData(data, verbose, overwriteTotal) {
    var result = {
        total: overwriteTotal ?? data.length,
        addition: 0,
        deletion: 0
    };

    for (var d in data) {
        if (!previousData.includes(data[d])) {
            result.addition++;
            if (verbose) {
                console.log("+ " + data[d])
            }
        }
    }
    for (var p in previousData) {
        if (!data.includes(previousData[p])) {
            result.deletion++;
            if (verbose) {
                console.log("- " + data[d])
            }
        }
    }
    previousData = data;
    return result;
}