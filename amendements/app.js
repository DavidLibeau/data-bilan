fs = require('fs-extra');

var statsCount = {
    0: 0,
    1: 0
};
var statsSortLegislature = {
    0: {},
    1: {}
};
var statsSortMonth = {};


async function main() {
    var files = await fs.readdir("data/15");
    for (var f in files) {
        if (files[f].slice(0, 1) != ".") {
            var data = await fs.readJson("data/15/" + files[f], 'utf8');
            for (var a in data) {
                manageAmendement(data[a], 1);
            }
        }
    }

    var data = await fs.readJson("data/Amendements_XIV.json", 'utf8');
    for (var t in data["textesEtAmendements"]["texteleg"]) {
        if (data["textesEtAmendements"]["texteleg"][t]["amendements"]["amendement"]) {
            var count = data["textesEtAmendements"]["texteleg"][t]["amendements"]["amendement"].length;
            for (var a in data["textesEtAmendements"]["texteleg"][t]["amendements"]["amendement"]) {
                manageAmendement(data["textesEtAmendements"]["texteleg"][t]["amendements"]["amendement"][a], 0);
            }
        }
    }
    console.log(statsCount);
    console.log(statsSortMonth);
    for (var y in statsSortMonth) {
        console.log(y + ", " + (statsSortMonth[y]["Adopté"] ? statsSortMonth[y]["Adopté"] : 0) + ", " + (statsSortMonth[y]["Rejeté"] ? statsSortMonth[y]["Rejeté"] : 0) + ", " + (statsSortMonth[y]["Retiré"] ? statsSortMonth[y]["Retiré"] : 0) + ", " + (statsSortMonth[y]["Tombé"] ? statsSortMonth[y]["Tombé"] : 0) + ", " + (statsSortMonth[y]["Non soutenu"] ? statsSortMonth[y]["Non soutenu"] : 0) + ", " + (statsSortMonth[y]["A discuter"] ? statsSortMonth[y]["A discuter"] : 0) + ", " + (statsSortMonth[y]["Irrecevable"] ? statsSortMonth[y]["Irrecevable"] : 0));
        //"Adopté", "Rejeté", "Retiré", "Tombé", "Non soutenu", "A discuter", "Irrecevable"
    }
    console.log(statsSortLegislature);
    for (var l in statsSortLegislature) {
        console.log(l + ", " + statsSortLegislature[l]["Adopté"] + ", " + statsSortLegislature[l]["Rejeté"] + ", " + statsSortLegislature[l]["Retiré"] + ", " + statsSortLegislature[l]["Tombé"] + ", " + statsSortLegislature[l]["Non soutenu"] + ", " + statsSortLegislature[l]["A discuter"] + ", " + statsSortLegislature[l]["Irrecevable"]);
        //"Adopté", "Rejeté", "Retiré", "Tombé", "Non soutenu", "A discuter", "Irrecevable"
    }
}
main();

function manageAmendement(data, legislatureId) {
    if (!data) {
        console.log(data);
    } else {
        var date;
        var sort;
        switch (legislatureId) {
            case 1:
                date = dateYear(data["amendement"]["cycleDeVie"]);
                //sort = data["amendement"]["cycleDeVie"]["sort"];
                sort = data["amendement"]["cycleDeVie"]["etatDesTraitements"]["etat"]["libelle"];
                if (sort == "Discuté") {
                    sort = data["amendement"]["cycleDeVie"]["etatDesTraitements"]["sousEtat"]["libelle"];
                }
                break;
            case 0:
                date = dateYear(data);
                if (data["sort"]) {
                    sort = data["sort"]["sortEnSeance"];
                } else {
                    sort = data["etat"];
                }
                break;
        }
        if (!sort || !date) {
            console.log("64", data);
        } else {
            if (sort.split(" ")[0] == "Irrecevable") {
                sort = "Irrecevable";
            }
            if (sort != "En traitement" && sort != "En recevabilité") {
                statsCount[legislatureId]++;
                if (!statsSortLegislature[legislatureId][sort]) {
                    statsSortLegislature[legislatureId][sort] = 0;
                }
                statsSortLegislature[legislatureId][sort]++;
                if (!statsSortMonth[date]) {
                    statsSortMonth[date] = {};
                }
                if (!statsSortMonth[date][sort]) {
                    statsSortMonth[date][sort] = 0;
                }
                statsSortMonth[date][sort]++;
            }
        }
    }
}

function dateYear(data) {
    if (data) {
        if (data["dateDepot"] && !data["dateDepot"]["@xsi:nil"]) {
            var dateArray = data["dateDepot"].split("-");
        } else if (data["datePublication"] && !data["datePublication"]["@xsi:nil"]) {
            var dateArray = data["datePublication"].split("-");
        } else {
            console.log('72', data);
            return undefined;
        }
        if (dateArray.length) {
            return dateArray[0];// + "-" + dateArray[1];
        } else {
            console.log('78', data);
        }
    }
    return undefined;
}