const parse = require('csv-parse');
const fs = require('fs-extra');

const processFile = async (path) => {
    records = []
    const parser = fs
        .createReadStream(path)
        .pipe(parse({
            // CSV options if any
            delimiter: ";",
            columns: true
        }));
    for await (const record of parser) {
        records.push(record)
    }
    return records
}

var mimeStats = {};
var mimeStatsRaw = {};
var orgStats = {};
var topOrgStats = {};
var org = {};
var datasetStats = {};
var datasetStatsFromRessource = {};
var totalDatasets = {};
var totalDatasetsFromRessource = {};
var mimeStats2020 = {};
var mimeStatsRaw2020 = {};
var formatStats = {};
var filetypeStats = {};
var spamUrls = "";
var spamOrgs = {};
var spamDownloads = {};

(async () => {
    var organizations = await processFile("export-organization-20210910-075012.csv");
    var datasets = await processFile("export-dataset-20210910-074533.csv");
    var ressources = await processFile("export-resource-20210910-074859.csv");
    for (var r in organizations) {
        org[organizations[r]['id']] = organizations[r];
    }
    /*for (var i = 0; i < 10; i++) {
        console.log(ressources[i]);
    }*/
    for (var r in ressources) {
        var annee = ressources[r]['created_at'].split("-")[0];
        var mime = ressources[r]['mime'];
        var mimeRaw = ressources[r]['mime'];

        if (mime.search("opendocument") != -1 || mime.search("officedocument") != -1) {
            mime = "opendocument";
        } else if (mime.search("pdf") != -1) {
            mime = "pdf";
        } else if (mime.search("html") != -1) {
            mime = "html";
        } else if (mime.search("image") != -1) {
            mime = "image";
        } else if (mime.search("json") != -1) {
            mime = "json";
        } else if (mime.search("xml") != -1) {
            mime = "xml";
        } else if (mime.search("csv") != -1) {
            mime = "csv";
        } else if (mime.search("xlsx") != -1 || mime.search("excel") != -1) {
            mime = "xlsx";
        } else if (mime.search("text") != -1) {
            mime = "text";
        } else {
            mime = "autres";
        }

        if (!mimeStats[annee]) {
            mimeStats[annee] = {};
        }
        if (!mimeStats[annee][mime]) {
            mimeStats[annee][mime] = 0;
        }
        mimeStats[annee][mime]++;

        if (!mimeStatsRaw[annee]) {
            mimeStatsRaw[annee] = {};
        }
        mimeStatsRaw[annee][mimeRaw]++;
        if (!mimeStatsRaw[annee][mimeRaw]) {
            mimeStatsRaw[annee][mimeRaw] = 0;
        }
        mimeStatsRaw[annee][mimeRaw]++;

        if (!filetypeStats[annee]) {
            filetypeStats[annee] = {};
        }
        if (!filetypeStats[annee][ressources[r]['filetype']]) {
            filetypeStats[annee][ressources[r]['filetype']] = 0;
        }
        filetypeStats[annee][ressources[r]['filetype']]++;

        if (!formatStats[annee]) {
            formatStats[annee] = {};
        }
        if (!formatStats[annee][ressources[r]['format']]) {
            formatStats[annee][ressources[r]['format']] = 0;
        }
        formatStats[annee][ressources[r]['format']]++;
        
        if(!datasetStatsFromRessource[annee]){
            datasetStatsFromRessource[annee] = [];
        }
        datasetStatsFromRessource[annee][ressources[r]["dataset.id"]] = ressources[r]["dataset.id"];
        totalDatasetsFromRessource[ressources[r]["dataset.id"]] = ressources[r]["dataset.id"];

        //2020     
        // dataset.id	dataset.title	dataset.slug	dataset.url	dataset.organization	dataset.organization_id	dataset.license	dataset.private	id	url	title	description	filetype	format	mime	filesize	checksum.type	checksum.value	created_at	modified	downloads
        if (annee == "2020") {
            if (!mimeStats2020[mime]) {
                mimeStats2020[mime] = 0;
            }
            mimeStats2020[mime]++;

            if (!mimeStatsRaw2020[mimeRaw]) {
                mimeStatsRaw2020[mimeRaw] = 0;
            }
            mimeStatsRaw2020[mimeRaw]++;

            if (mimeRaw == "") {
                spamUrls += ressources[r]['url'] + "\n";

                if (!spamOrgs[ressources[r]['dataset.organization']]) {
                    spamOrgs[ressources[r]['dataset.organization']] = 0;
                }
                spamOrgs[ressources[r]['dataset.organization']]++;

                if (!spamDownloads[ressources[r]['downloads']]) {
                    spamDownloads[ressources[r]['downloads']] = 0;
                }
                spamDownloads[ressources[r]['downloads']]++;
            }
        }
    }
    for (var d in datasets) {
        var annee = datasets[d]['created_at'].split("-")[0];
        if (!datasetStats[annee]) {
            datasetStats[annee] = 0;
        }
        datasetStats[annee]++;

        //org
        var organization_id = datasets[d]['organization_id'];
        if (organization_id) {
            var orgName = organization_id;
            if (org[organization_id]) {
                orgName = org[organization_id]['name'];
            }
            if (!orgStats[annee]) {
                orgStats[annee] = {};
            }
            if (!orgStats[annee][orgName]) {
                orgStats[annee][orgName] = 0;
            }
            orgStats[annee][orgName]++;
        } else {
            console.log("error", datasets[d]);
        }
        totalDatasets[datasets[d]["id"]] = datasets[d]["id"];
    }
    for (var annee in orgStats) {
        topOrgStats[annee] = {
            value: 0,
            name: ""
        };
        for (var o in orgStats[annee]) {
            if (topOrgStats[annee].value < orgStats[annee][o]) {
                topOrgStats[annee].value = orgStats[annee][o];
                topOrgStats[annee].name = o;
            }
        }
    }
    /*console.log("annee, json, xml, csv, xlsx, pdf, html, opendocument, image, autres");
    for (var annee in mimeStats) {
        console.log(annee + ", " + mimeStats[annee]['json'] + ", " + mimeStats[annee]['xml'] + ", " + mimeStats[annee]['csv'] + ", " + mimeStats[annee]['xlsx'] + ", " + mimeStats[annee]['pdf'] + ", " + mimeStats[annee]['html'] + ", " + mimeStats[annee]['opendocument'] + ", " + mimeStats[annee]['image'] + ", " + mimeStats[annee]['autres'])
    }*/
    /*console.log("org, datasets");
    for (var orgName in orgStats[2020]) {
        console.log("\"" + orgName + "\", " + orgStats[2020][orgName])
    }*/
    /*console.log("annee, datasets");
    for (var annee in datasetStats) {
        console.log(annee + ", " + datasetStats[annee])
    }*/
    /*console.log("annee, org");
    for (var annee in topOrgStats) {
        console.log(annee + ", " + topOrgStats[annee]['name'])
    }*/
    console.log("annee, remote, file");
    for (var annee in filetypeStats) {
        console.log(annee + ", " + filetypeStats[annee]['remote'] + ", " + filetypeStats[annee]['file'])
    }
    //{"xlsx":1736,"html":3522,"csv":6478,"zip":23514,"pdf":2924,"xml":552,"json":26273,"shp":25118,"web page":323,"geojson":587,"arcgis geoservices rest api":248,"kml":516,"esri rest":80,"tiff":96,"geopackage":11,"bat":18,"xlb":50,"xls":1923,"odt":11,"document":14930,"5-2018-fauvelles.csv":1,"5-2019-snbs.csv":1,"5-2019-fauvelles.csv":1,"5-2018-snbs.csv":1,"":4907,"wms":3026,"wfs":1398,"sl3":6,"png":8,"video":2,"-site-internet.pdf":1,"-du-ccas.pdf":1,"txt":166,"-depenses-fetes-et-ceremonies-usages.pdf":1,"-mandat-cdg-77.pdf":1,"-mandat-cdg-77-assurance-statutaire.pdf":1,"gouv.xlsx":2,"dbf":299,"prj":10,"cpg":4,"url":2,"5.xlsx":1,"7z":57,"gpkg":68,"shapefile":19,"gouv12.2019-csv.csv":1,"ods":71,"docx":137,"css":4,"schema.json":2,"ecw":17,"gtfs.zip":4,"geotiff":1,"shp.zip":1,"odata":20,"gtfs-rt":23,"gtfs":50,".csv":1,"openair":1,"ogc wms":40,"h5":9,"geom06.bruit-aerien-iso5db-.xml":1,"bin":1,"wcs":6,"rar":124,"gbfs":3,"gtfsrt.pb":1,"gtfsrt.json":1,"neptune.zip":1,"qgis":224,"application/rtf":127,"neptune":6,"maximilien.fr-2021-09-05t15-20-00.xml":1,"image":1229,
    console.log("annee, json, xml, csv, html, xlsx, pdf, zip, shp, document, NaN");
    for (var annee in formatStats) {
        console.log(annee + ", " + formatStats[annee]['json']+ ", " + formatStats[annee]['xml']+ ", " + formatStats[annee]['csv']+ ", " + formatStats[annee]['html']+ ", " + formatStats[annee]['xlsx']+ ", " + formatStats[annee]['pdf']+ ", " + formatStats[annee]['zip']+ ", " + formatStats[annee]['shp']+ ", " + (formatStats[annee]['document'] ? formatStats[annee]['document'] : 0 )+ ", " + formatStats[annee][""])
    }
    
    console.log(Object.keys(totalDatasets).length);
    console.log(Object.keys(totalDatasetsFromRessource).length);
    
    fs.writeFile("mimeStats.json", JSON.stringify(mimeStats), function (err) {
        if (err) {
            return console.log(err);
        }
        console.log("mimeStats saved!");
    });
    fs.writeFile("mimeStatsRaw.json", JSON.stringify(mimeStatsRaw), function (err) {
        if (err) {
            return console.log(err);
        }
        console.log("mimeStatsRaw saved!");
    });
    fs.writeFile("orgStats.json", JSON.stringify(orgStats), function (err) {
        if (err) {
            return console.log(err);
        }
        console.log("orgStats saved!");
    });
    fs.writeFile("datasetStats.json", JSON.stringify(datasetStats), function (err) {
        if (err) {
            return console.log(err);
        }
        console.log("datasetStats saved!");
    });
    fs.writeFile("topOrgStats.json", JSON.stringify(topOrgStats), function (err) {
        if (err) {
            return console.log(err);
        }
        console.log("topOrgStats saved!");
    });

    // 2020
    fs.writeFile("mimeStats2020.json", JSON.stringify(mimeStats2020), function (err) {
        if (err) {
            return console.log(err);
        }
        console.log("mimeStats2020 saved!");
    });
    fs.writeFile("mimeStatsRaw2020.json", JSON.stringify(mimeStatsRaw2020), function (err) {
        if (err) {
            return console.log(err);
        }
        console.log("mimeStatsRaw2020 saved!");
    });
    fs.writeFile("spamOrgs.json", JSON.stringify(spamOrgs), function (err) {
        if (err) {
            return console.log(err);
        }
        console.log("spamOrgs saved!");
    });
    fs.writeFile("spamDownloads.json", JSON.stringify(spamDownloads), function (err) {
        if (err) {
            return console.log(err);
        }
        console.log("spamDownloads saved!");
    });
    fs.writeFile("spamUrls.csv", spamUrls, function (err) {
        if (err) {
            return console.log(err);
        }
        console.log("spamUrls saved!");
    });
    fs.writeFile("formatStats.json", JSON.stringify(formatStats), function (err) {
        if (err) {
            return console.log(err);
        }
        console.log("formatStats saved!");
    });
    fs.writeFile("filetypeStats.json", JSON.stringify(filetypeStats), function (err) {
        if (err) {
            return console.log(err);
        }
        console.log("filetypeStats saved!");
    });
    
    for(var annee in datasetStatsFromRessource){
        var keys = Object.keys(datasetStatsFromRessource[annee]);
        datasetStatsFromRessource[annee] = keys.length;
    }
    fs.writeFile("datasetStatsFromRessource.json", JSON.stringify(datasetStatsFromRessource), function (err) {
        if (err) {
            return console.log(err);
        }
        console.log("datasetStatsFromRessource saved!");
    });
})()