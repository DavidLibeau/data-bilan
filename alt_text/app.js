const csvParse = require('csv-parse');
const fs = require('fs-extra')

var stats = {};
var statsYear = {};
var statsYearHandi = {};
var totalTweets = 0;

async function processFile(id) {
    const parser = fs
        .createReadStream("./alt_texts" + id + ".csv")
        .pipe(csvParse({
            columns: true
        }));
    for await (const record of parser) {
        var year = record.local_time.split("-")[0];
        if (year >= 2016) {
            totalTweets++;
            if (!statsYear[year]) {
                statsYear[year] = {
                    "images": 0,
                    "alt_texts": 0,
                    "total": 0
                };
            }
            if (!stats[record.user_screen_name]) {
                stats[record.user_screen_name] = {
                    "images": 0,
                    "alt_texts": 0,
                    "total": 0
                };
            }
            if (record.user_screen_name == "handicap_gouv") {
                if (!statsYearHandi[year]) {
                    statsYearHandi[year] = {
                        "images": 0,
                        "alt_texts": 0,
                        "total": 0
                    };
                }
            }
            var media_types = record.media_types.split("|");
            if (media_types.indexOf("photo") != -1) {
                stats[record.user_screen_name].images++;
                statsYear[year].images++;
                if (record.user_screen_name == "handicap_gouv") {
                    statsYearHandi[year].images++;
                }
            }
            var alt_texts = record.alt_texts.split("|");
            var alt_text_empty = true;
            for (var a in alt_texts) {
                if (alt_texts[a] != "") {
                    alt_text_empty = false;
                }
            }
            if (!alt_text_empty) {
                stats[record.user_screen_name].alt_texts++;
                statsYear[year].alt_texts++;
                if (record.user_screen_name == "handicap_gouv") {
                    statsYearHandi[year].alt_texts++;

                }
            }
            stats[record.user_screen_name].total++;
            statsYear[year].total++;

            if (record.user_screen_name == "handicap_gouv") {
                statsYearHandi[year].total++;
            }
        }
    }
}

async function main() {
    console.log("Running…");
    for (var i = 1; i <= 4; i++) {
        console.log(i);
        await processFile(i);
    }
    console.log("Done.");
    console.log(stats);
    for (var s in stats) {
        console.log(s + "," +(stats[s].alt_texts / stats[s].images * 100));
    }
    console.log(statsYear);
    for (var y in statsYear) {
        console.log(y + "," + Math.round((statsYear[y].alt_texts / statsYear[y].images * 100) * 100) / 100);
    }
    console.log(statsYearHandi);
    for (var y in statsYearHandi) {
        console.log(y + "," + Math.round((statsYearHandi[y].alt_texts / statsYearHandi[y].images * 100) * 100) / 100) + "%," + statsYearHandi[y].alt_texts + "," + statsYearHandi[y].images + "," + statsYearHandi[y].total;
    }
    console.log("Total tweets : "+totalTweets);
}

main();