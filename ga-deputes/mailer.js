import fs from "fs-extra";
import nodemailer from "nodemailer";

var deputes = {};

let transporter = nodemailer.createTransport({
    host: "",
    port: 465,
    secure: true, // true for 465, false for other ports
    auth: {
        user: "",
        pass: "",
    },
});

fs.readJson("scrap.json", async function (err, object) {
    if (err) return console.log(err);
    for (var o in object) {
        var depute = object[o];
        deputes[[depute["nom"]]] = depute;
    }

    fs.readJson("analyse.json", async function (err, object) {
        if (err) return console.log(err);
        for (var nom in object) {
            var emails = [];
            for (var e in deputes[nom].emails) {
                emails.push(deputes[nom].emails[e].email);
            }
            var sites = object[nom];
            var message = "A l'attention de l'équipe du député " + nom + " :\n\nMadame, Monsieur,\n\nJe souhaite vous alerter sur la non conformité au RGPD du site web de votre député. D'après mon analyse, celui-ci utiliserait toujours ";
            for (var site in sites) {
                var ga = false;
                var gc = false;
                if (sites[site].includes("google-analytics")) {
                    ga = true;
                }
                if (sites[site].includes("google-captcha")) {
                    gc = true;
                }
                if (ga && gc) {
                    message += "Google Analytics et Google reCaptcha ";
                } else {
                    if (ga) {
                        message += "Google Analytics ";
                    }
                    if (gc) {
                        message += "Google reCaptcha ";
                    }
                }
                message += "sur le site " + site;
                if (Object.keys(sites).length > 1) {
                    message += ",";
                }
                message += " ";
            }
            message += "de façon non conforme au RGPD et à la Loi informatique et libertés tel que la CNIL l'a confirmé dans plusieurs de ses décisions (";
            var gaSource = "concernant Google Analytics : https://www.cnil.fr/fr/cookies-et-autres-traceurs/regles/google-analytics-et-transferts-de-donnees-comment-mettre-son-outil-de-mesure-daudience-en-conformite";
            var gcSource = "concernant Google reCAPTCHA : https://twitter.com/DavidLibeau/status/1516041376542208012";
            var ga = false;
            var gc = false;
            for (var site in sites) {
                if (sites[site].includes("google-analytics")) {
                    ga = true;
                }
                if (sites[site].includes("google-captcha")) {
                    gc = true;
                }
            }
            if (ga && gc) {
                message += gaSource + " et " + gcSource;
            } else {
                if (ga) {
                    message += gaSource;
                }
                if (gc) {
                    message += gcSource;
                }
            }
            message += ").\nJe souhaite savoir dans combien de temps le site web du député sera conforme aux dispositions en vigueur et protègera les données des électeurs. Votre réponse pourra figurer dans un article sur mon blog (https://blog.davidlibeau.fr).\n\nBien cordialement,\nDavid Libeau";
            console.log(message);
            console.log("-----------------");
            console.log(emails.join(", "));
            let info = await transporter.sendMail({
                from: '"David Libeau" <>', // sender address
                to: emails.join(", "), // list of receivers
                subject: "Conformité RGPD du site web du député", // Subject line
                text: message, // plain text body
                html: "", // html body
            });
            console.log("Message envoyé : %s", info.messageId);
            console.log("-----------------");
        }
    });
});