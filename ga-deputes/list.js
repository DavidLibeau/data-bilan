import fs from "fs-extra";

var deputes = {};
var count = 1;


fs.readJson("scrap.json", async function (err, object) {
    if (err) return console.log(err);
    for (var o in object) {
        var depute = object[o];
        deputes[[depute["nom"]]] = depute;
    }

    fs.readJson("analyse.json", async function (err, object) {
        if (err) return console.log(err);
        for (var nom in object) {
            var emails = [];
            for (var e in deputes[nom].emails) {
                emails.push(deputes[nom].emails[e].email);
            }
            var sites = object[nom];
            var message = count + "," + nom ;
            for (var site in sites) {
                message += ","+site + " : "
                if (sites[site].includes("google-analytics")) {
                    message += "google-analytics "
                }
                if (sites[site].includes("google-captcha")) {
                    message += "google-captcha "
                }
                
            }
            console.log(message);
            count++;
        }
    });
});