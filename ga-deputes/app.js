import fs from "fs-extra"
import puppeteer from 'puppeteer';
var browser = await puppeteer.launch({
    args: ['--no-sandbox', '--disable-setuid-sandbox'],
});
var data = {};
var count = 0;

fs.readJson("nosdeputes.fr_deputes_2023-02-01.json", async function(err, object) {
    if (err) return console.log(err);
    for (var o in object["deputes"]) {
        var depute = object["deputes"][o]["depute"];
        for (var s in depute["sites_web"]) {
            var site = depute["sites_web"][s]["site"];
            if (site.search(/twitter|facebook|instagram|linktr|linkr.bio|youtube|actionpopulaire|t.me|lesecologistes2022|framaforms|europa.eu|linkedin|gouvernement|maregionsud|editionsatelier|rassemblementnational|gouv.fr|avecvous|mediapart/i) == -1) {
                await scrapDomain(site, depute);
            }
        }
    }
});


async function scrapDomain(domain, depute) {
    console.log(domain);
    var page = await browser.newPage();
    var requests = [];
    page.on('requestfinished', (r) => {
        requests.push(r.url());
    });
    if (!data[depute.nom]) {
        data[depute.nom] = {
            "nom": depute.nom,
            "emails": depute.emails,
            "groupe_sigle": depute.groupe_sigle,
            "sites_web": {}
        };
    }
    try {
        await page.goto(domain, {
            waitUntil: 'load',
            timeout: 0
        });
        //await page.waitForNetworkIdle();
        await page.waitForTimeout(10000);
        var cookies = await page.cookies();
        var links = await page.$$eval("a", el => el.map(x => x.getAttribute("href")));
        var localStorage = await page.evaluate(() => {
            let json = {};
            for (let i = 0; i < localStorage.length; i++) {
                const key = localStorage.key(i);
                json[key] = localStorage.getItem(key);
            }
            return json;
        });
        data[depute.nom]["sites_web"][domain] = {
            depute: {
                "nom": depute.nom,
                "emails": depute.emails,
                "groupe_sigle": depute.groupe_sigle,
            },
            cookies: cookies,
            localStorage: localStorage,
            links: links,
            requests: requests
        };
        await page.close();
    } catch (e) {
        data[depute.nom]["sites_web"][domain] = {
            error: e.message
        }
        try {
            await page.close();
        } catch (e) {}
    }
    count++;
    console.log(count); //Total : 384
    await fs.writeFile("scrap.json", JSON.stringify(data));
}