import fs from "fs-extra"
var data = {};

fs.readJson("scrap.json", async function (err, object) {
    if (err) return console.log(err);
    for (var o in object) {
        var depute = object[o];
        for (var s in depute["sites_web"]) {
            var site = depute["sites_web"][s];
            if (s.search(/mlafrance|franceinsoumise|deputes-rn|twitch/) == -1) {
                for (var r in site["requests"]) {
                    var request = site["requests"][r];
                    if (request.search("google-analytics")!=-1) {
                        if (!data[depute["nom"]]) {
                            data[depute["nom"]] = {};
                        }
                        if (!data[depute["nom"]][s]) {
                            data[depute["nom"]][s] = [];
                        }
                        data[depute["nom"]][s].push("google-analytics");
                    }
                    if (request.search("google.*captcha")!=-1) {
                        if (!data[depute["nom"]]) {
                            data[depute["nom"]] = {};
                        }
                        if (!data[depute["nom"]][s]) {
                            data[depute["nom"]][s] = [];
                        }
                        data[depute["nom"]][s].push("google-captcha");
                    }
                }
            }

        }
    }
    console.log(Object.keys(data).length);
    await fs.writeFile("analyse.json", JSON.stringify(data));
});