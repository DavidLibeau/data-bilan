const puppeteer = require('puppeteer');
var browser;
const fs = require('fs-extra');
const got = require('got');

var data = {};
var count = 0;
var split = 8;
var scrapStarted = 0;
var scrapInterval;
var domains = [];

(async () => {
    console.log("Starting…");
    browser = await puppeteer.launch({
        args: ['--no-sandbox', '--disable-setuid-sandbox'],
    });
    var data = await fs.readFile("top1000-fr-dataforseocom.json", 'utf8');
    data = JSON.parse(data);
    for await (const record of data) {
        domains.push(record.domain);
    }
    console.log(domains);
    console.log(domains.length + " domains to scrap. Starting scraping…");
    startScrap();
})();

async function scrapDomain(domain) {
    console.log(domain);
    var allowedToScrap = true;
    try {
        var robotsTxt = await got('http://' + domain + '/robots.txt');
        robotsTxt = robotsTxt.body;
    } catch (e) {}
    if (robotsTxt) {
        var robotsArr = robotsTxt.split("\n");
        var currentUserAgent;
        for (var l in robotsArr) {
            var line = robotsArr[l].split(": ");
            if (line[0] == "User-agent") {
                currentUserAgent = line[1];
            } else if (currentUserAgent == "*") {
                if (line[0] == "Disallow" && line[1] == "/") {
                    allowedToScrap = false;
                }
            }
        }
    }
    if (allowedToScrap) {
        var page = await browser.newPage();
        var requests = [];
        page.on('requestfinished', (r) => {
            requests.push(r._url);
        });
        try {
            await page.goto('http://' + domain, {
                waitUntil: 'load',
                timeout: 0
            });
            //await page.waitForNetworkIdle();
            await page.waitForTimeout(10000);
            var cookies = await page.cookies();
            var links = await page.$$eval("a", el => el.map(x => x.getAttribute("href")));
            var localStorage = await page.evaluate(() => {
                let json = {};
                for (let i = 0; i < localStorage.length; i++) {
                    const key = localStorage.key(i);
                    json[key] = localStorage.getItem(key);
                }
                return json;
            });
            data[domain] = {
                cookies: cookies,
                localStorage: localStorage,
                links: links,
                requests: requests,
                robotTxt: robotsTxt
            };
            await page.close();
        } catch (e) {
            data[domain] = {
                error: e.message
            }
            try {
                await page.close();
            } catch (e) {}
        }
    } else {
        data[domain] = {
            robotTxt: robotsTxt
        }
    }
    count++;
    if (count / split == 10000) {
        console.log(count + " websites done. Saving data and continuing…");
        await fs.writeFile("data-" + split + ".json", JSON.stringify(data));
        data = {};
        split++;
    }
}

function startScrap() {
    scrapInterval = setInterval(launchNextScrap, 200);
}

async function launchNextScrap() {
    if (scrapStarted >= domains.length) {
        stopScrap();
    } else {
        var pages = await browser.pages();
        if (pages.length < 20) {
            scrapDomain(domains[scrapStarted]);
            scrapStarted++;
        }
    }
}

function stopScrap() {
    clearInterval(scrapInterval);
    setTimeout(async function () {
        await browser.close();
        console.log("Saving last data…");
        await fs.writeFile("data-last.json", JSON.stringify(data));
        console.log("Done.");
    }, 20000);
}