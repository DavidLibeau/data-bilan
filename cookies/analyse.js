const fs = require('fs-extra');
const got = require('got');

var domainsWithError = [];
var total = 0;
var statsSitesWithCookies = 0;
var statsSitesWithLocalStorage = 0;
var statsSitesWithCookiesOrLocalStorage = 0;
var statsCookieCount = [];
var statsLocalStorageCount = [];
var statsCookieName = {};
var statsLocalStorageName = {};
var statsExternalProvider = {};

(async () => {
    console.log("Starting…");
    var data = await fs.readFile("data-last.json", 'utf8');
    data = JSON.parse(data);
    for (var domain in data) {
        var localExternalProvider = {};
        if (data[domain].error) {
            console.log(data[domain].error);
            domainsWithError.push(domain);
        } else if (data[domain].cookies) {
            total++;
        }
        if (data[domain].cookies && data[domain].cookies.length > 0) {
            statsCookieCount.push(data[domain].cookies.length);
            statsSitesWithCookies++;
            for (var c in data[domain].cookies) {
                switch (data[domain].cookies[c].name) {
                    case '_gat':
                    case '_gid':
                    case '_gat':
                    case '_gcl_au':
                    case '__gads':
                    case '__utmb':
                    case '__utmz':
                    case '__utmc':
                    case '__utma':
                    case '__utmt':
                        localExternalProvider["Google"] = 1;
                        break;
                    case 'atauthority':
                    case 'atuserid':
                        localExternalProvider["AT Internet (Xiti)"] = 1;
                        break;
                    case '_fbp':
                        localExternalProvider["Facebook"] = 1;
                        break;
                    case 'OptanonConsent':
                        localExternalProvider["OneTrust"] = 1;
                        break;
                    case '__cf_bm':
                        localExternalProvider["Cloudflare"] = 1;
                        break;
                    case 'datadome':
                        localExternalProvider["Datadome"] = 1;
                        break;
                    case 'tarteaucitron':
                        localExternalProvider["Tarteaucitron"] = 1;
                        break;
                    case '_sp_v1_lt':
                    case '_sp_v1_ss':
                    case '_sp_v1_consent':
                    case '_sp_v1_opt':
                    case '_sp_v1_data':
                    case '_sp_v1_uid':
                    case '_sp_v1_csv':
                        localExternalProvider["Microsoft sharepoint"] = 1;
                        break;
                }
                if (!statsCookieName[data[domain].cookies[c].name]) {
                    statsCookieName[data[domain].cookies[c].name] = 0;
                }
                statsCookieName[data[domain].cookies[c].name]++;
            }
        } else {
            statsCookieCount.push(0);
        }
        if (data[domain].localStorage) {
            var localStorageKeys = Object.keys(data[domain].localStorage);
            if (localStorageKeys.length > 0) {
                statsSitesWithLocalStorage++;
                statsLocalStorageCount.push(localStorageKeys.length);
                for (var l in localStorageKeys) {
                    if (!statsLocalStorageName[localStorageKeys[l]]) {
                        statsLocalStorageName[localStorageKeys[l]] = 0;
                    }
                    statsLocalStorageName[localStorageKeys[l]]++;
                    switch (localStorageKeys[l]) {
                        case 'google_experiment_mod57':
                        case 'google_experiment_mod37':
                        case 'goog_pem_mod':
                        case 'google_experiment_mod56':
                        case 'google_experiment_mod36':
                        case 'google_experiment_mod53':
                        case 'google_experiment_mod44':
                        case 'google_experiment_mod34':
                        case 'google_auto_fc_cmp_setting':
                        case 'google_adsense_settings':
                        case 'google_ama_config':
                        case 'google_ama_settings':
                            localExternalProvider["Google"] = 1;
                            break;
                        case 'com.adobe.reactor.dataElementCookiesMigrated':
                            localExternalProvider["Adobe"] = 1;
                            break;
                        case 'sddan:cmp':
                            localExternalProvider["Sirdata"] = 1;
                            break;
                        case '_uetsid_exp':
                        case '_uetvid':
                        case '_uetvid_exp':
                        case '_uetsid':
                            localExternalProvider["Bing"] = 1;
                            break;
                        case 'ABTastyData':
                            localExternalProvider["Bing"] = 1;
                            break;
                        case '_cb_svref':
                        case '_cb':
                        case '_chartbeat2':
                        case '_cb_svref_expires':
                        case '_chartbeat2_expires':
                        case '_cb_expires':
                            localExternalProvider["Chartbeat"] = 1;
                            break;
                    }
                }
            }
        }
        for (var provider in localExternalProvider) {
            if (!statsExternalProvider[provider]) {
                statsExternalProvider[provider] = 0;
            }
            statsExternalProvider[provider]++;
        }
        if(data[domain].cookies && data[domain].cookies.length > 0 || (data[domain].localStorage && Object.keys(data[domain].localStorage).length > 0)){
            statsSitesWithCookiesOrLocalStorage++;
        }
    }
    console.log(domainsWithError);
    console.log(domainsWithError.length + " domains with error.");
    console.log(total);
    console.log("statsSitesWithCookies:" + statsSitesWithCookies);
    console.log("statsSitesWithLocalStorage:" + statsSitesWithLocalStorage);
    console.log("statsSitesWithCookiesOrLocalStorage:" + statsSitesWithCookiesOrLocalStorage);
    console.log("statsCookieCount mean:" + average(statsCookieCount));
    var chartStatsCookieCount = {};
    for(var c in statsCookieCount){
        if(!chartStatsCookieCount[statsCookieCount[c]]){
            chartStatsCookieCount[statsCookieCount[c]] = 0;
        }
        chartStatsCookieCount[statsCookieCount[c]]++;
    }
    console.log(chartStatsCookieCount);
    console.log("statsCookieCount mean:" + average(statsLocalStorageCount));
    console.log("--- cookie names:");
    var statsCookieNameArr = [];
    for (var key in statsCookieName) statsCookieNameArr.push([key, statsCookieName[key]]);
    statsCookieNameArr.sort(function (a, b) {
        a = a[1];
        b = b[1];

        return a > b ? -1 : (a > b ? 1 : 0);
    });
    for (var i = 0; i < 100; i++) {
        console.log(statsCookieNameArr[i][0]+","+statsCookieNameArr[i][1]);
    }
    console.log("--- localStorage names:");
    var statsLocalStorageNameArr = [];
    for (var key in statsLocalStorageName) statsLocalStorageNameArr.push([key, statsLocalStorageName[key]]);
    statsLocalStorageNameArr.sort(function (a, b) {
        a = a[1];
        b = b[1];

        return a > b ? -1 : (a > b ? 1 : 0);
    });
    for (var i = 0; i < 100; i++) {
        console.log(statsLocalStorageNameArr[i][0]+","+statsLocalStorageNameArr[i][1]);
    }
    console.log("--- external providers:");
    var statsExternalProviderArr = [];
    for (var key in statsExternalProvider) statsExternalProviderArr.push([key, statsExternalProvider[key]]);
    statsExternalProviderArr.sort(function (a, b) {
        a = a[1];
        b = b[1];

        return a > b ? -1 : (a > b ? 1 : 0);
    });
    for (var i = 0; i < 10; i++) {
        console.log(statsExternalProviderArr[i][0]+","+statsExternalProviderArr[i][1]);
    }
})();

const average = (array) => array.reduce((a, b) => a + b) / array.length;