fs = require('fs');

var statsCC = {};
var statsProcedure = {};
var statsOrdonnanceLegislature = {};
var statsOrdonnanceAnnee = {};

fs.readdir("data/dossierParlementaire", (err, files) => {
    if (err) {
        throw err;
    }

    files.forEach(file => {
        fs.readFile("data/dossierParlementaire/" + file, 'utf8', function (err, data) {
            if (err) {
                return console.log(err);
            }
            data = JSON.parse(data);
            manageData(data, 1);
        });
    });
});

fs.readFile("data/Dossiers_Legislatifs_XIV.json", 'utf8', function (err, data) {
    if (err) {
        return console.log(err);
    }
    data = JSON.parse(data);
    data = data["export"]["dossiersLegislatifs"]["dossier"];
    for (var d in data) {
        manageData(data[d], 0);
    }
});

setTimeout(function () {
    for (var s in statsCC) {
        console.log(s + ", " + (statsCC[s]["Conforme"] ? statsCC[s]["Conforme"] : 0) + ", " + (statsCC[s]["Conforme avec réserve"] ? statsCC[s]["Conforme avec réserve"] : 0) + ", " + (statsCC[s]["Partiellement conforme"] ? statsCC[s]["Partiellement conforme"] : 0) + ", " + (statsCC[s]["Non conforme"] ? statsCC[s]["Non conforme"] : 0));
    }
    fs.writeFile("statsCC.json", JSON.stringify(statsCC), function (err) {
        if (err) {
            return console.log(err);
        }
        console.log("statsCC saved!");
    });
    fs.writeFile("statsProcedure.json", JSON.stringify(statsProcedure), function (err) {
        if (err) {
            return console.log(err);
        }
        console.log("statsProcedure saved!");
    });
    fs.writeFile("statsOrdonnanceLegislature.json", JSON.stringify(statsOrdonnanceLegislature), function (err) {
        if (err) {
            return console.log(err);
        }
        console.log("statsOrdonnanceLegislature saved!");
    });
    fs.writeFile("statsOrdonnanceAnnee.json", JSON.stringify(statsOrdonnanceAnnee), function (err) {
        if (err) {
            return console.log(err);
        }
        console.log("statsOrdonnanceAnnee saved!");
    });
}, 5000);

function manageData(data, idLegislature) {
    data = data["dossierParlementaire"];
    var procedureParlementaire = data["procedureParlementaire"]["libelle"];
    if (data["actesLegislatifs"]) {
        var premierActeLegislatif;
        var actesLegislatifs;
        if (data["actesLegislatifs"]["acteLegislatif"][0]) {
            premierActeLegislatif = data["actesLegislatifs"]["acteLegislatif"][0]["actesLegislatifs"]["acteLegislatif"][0];
            if (!premierActeLegislatif && data["actesLegislatifs"]["acteLegislatif"][0]["actesLegislatifs"]["acteLegislatif"]) {
                premierActeLegislatif = data["actesLegislatifs"]["acteLegislatif"][0]["actesLegislatifs"]["acteLegislatif"];
            }
            actesLegislatifs = data["actesLegislatifs"]["acteLegislatif"];
        } else {
            premierActeLegislatif = data["actesLegislatifs"]["acteLegislatif"]["actesLegislatifs"]["acteLegislatif"];
            if (premierActeLegislatif["actesLegislatifs"] && premierActeLegislatif["actesLegislatifs"]["acteLegislatif"]) {
                premierActeLegislatif = premierActeLegislatif["actesLegislatifs"]["acteLegislatif"];
            }
            if (premierActeLegislatif[0]) {
                premierActeLegislatif = premierActeLegislatif[0];
            }
            actesLegislatifs = [premierActeLegislatif];
        }
        if (!premierActeLegislatif) {
            console.log(data)
            console.log(data["actesLegislatifs"]["acteLegislatif"][0])
        }
        var annee = "NaN";
        if (premierActeLegislatif["dateActe"]) {
            annee = premierActeLegislatif["dateActe"].split("-")[0];
        }
        if (annee == "NaN" || !annee) {
            console.log(premierActeLegislatif);
        }
        var conseilConstitConclusion = "NaN";
        for (var a in actesLegislatifs) {
            if (actesLegislatifs[a]["codeActe"] == "CC") {
                for (var b in data["actesLegislatifs"]["acteLegislatif"][a]["actesLegislatifs"]["acteLegislatif"]) {
                    if (data["actesLegislatifs"]["acteLegislatif"][a]["actesLegislatifs"]["acteLegislatif"][b]["statutConclusion"]) {
                        conseilConstitConclusion = data["actesLegislatifs"]["acteLegislatif"][a]["actesLegislatifs"]["acteLegislatif"][b]["statutConclusion"]["libelle"];
                        if (!conseilConstitConclusion) {
                            var missing = {
                                'TCD01': 'Conforme',
                                'TCD02': 'Partiellement conforme',
                                'TCD03': 'Conforme avec réserve',
                                'TCD04': 'Non conforme',
                                'TCD01,': 'Conforme',
                                'TCD02,': 'Partiellement conforme',
                                'TCD03,': 'Conforme avec réserve',
                                'TCD04,': 'Non conforme',
                            };
                            conseilConstitConclusion = missing[data["actesLegislatifs"]["acteLegislatif"][a]["actesLegislatifs"]["acteLegislatif"][b]["statutConclusion"]["fam_code"]];
                        }
                    }
                }
            }
        }
        if (!statsCC[annee]) {
            statsCC[annee] = {};
        }
        if (!statsCC[annee][conseilConstitConclusion]) {
            statsCC[annee][conseilConstitConclusion] = 0;
        }
        statsCC[annee][conseilConstitConclusion]++;

        if (!statsProcedure[idLegislature]) {
            statsProcedure[idLegislature] = {};
        }
        if (!statsProcedure[idLegislature][procedureParlementaire]) {
            statsProcedure[idLegislature][procedureParlementaire] = 0;
        }
        statsProcedure[idLegislature][procedureParlementaire]++;

        if (data["titreDossier"]["titreChemin"] && data["titreDossier"]["titreChemin"].search("ordonnance")) {
            if (!statsOrdonnanceLegislature[idLegislature]) {
                statsOrdonnanceLegislature[idLegislature] = 0;
            }
            statsOrdonnanceLegislature[idLegislature]++;
            
            if (!statsOrdonnanceAnnee[annee]) {
                statsOrdonnanceAnnee[annee] = 0;
            }
            statsOrdonnanceAnnee[annee]++;
        }
    } else {
        console.log("Error");
        console.log(data);
    }
}