const fs = require('fs-extra')

var extension = "json";

var merge = [];
var mergeId = 0;



async function manageDir(path) {
    var files = await fs.readdir(path);
    for (var f in files) {
        await managefile(files[f], path);
    }
}

async function managefile(file, path) {
    if (file.slice(0, 1) != ".") {
        if (file.slice(-1 - extension.length) == "." + extension) {
            var data = await fs.readFile(path + "/" + file, 'utf8');
            data = JSON.parse(data);
            merge.push(data);
            if(merge.length>=10000){
                await fs.writeFile("merge-"+mergeId+".json", JSON.stringify(merge));
                merge = [];
                mergeId++;
                console.log(mergeId);
            }
        } else {
            await manageDir(path + "/" + file);
        }
    }
}

async function main() {
    console.log("Running…");
    await manageDir("dist");
    await fs.writeFile("merge.json", JSON.stringify(merge));
    console.log(merge.length);
    console.log("Done.");
}

main();
console.log("-");