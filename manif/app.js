const fs = require('fs-extra');
const got = require('got');
const parse = require('node-html-parser');

var data = {};

(async () => {
    for (var a = 2012; a <= 2021; a++) {
        data[a] = {};
        for (var m = 1; m <= 12; m++) {
            data[a][m] = 0;
            var mois = m;
            if (m < 10) {
                mois = "0" + m;
            }
            var end = false;
            var page = 1;
            while (!end) {
                var pageUrl = "";
                if (page > 1) {
                    pageUrl = "page" + page + "/";
                }
                var url = 'https://www.bfmtv.com/archives/societe/manifestations/' + a + '/' + mois + '/' + pageUrl;
                try {
                    const response = await got(url);
                    const root = parse.parse(response.body);
                    var articles = root.querySelector('#archives_date_section .block_fleuve_line').childNodes;
                    for (var article in articles) {
                        if (articles[article].rawTagName == "article") {
                            data[a][m]++;
                        }
                    }
                    page++;
                } catch (error) {
                    //console.log(error);
                    //console.log(url);
                    end = true;
                    console.log(mois + "-" + a + ", " + data[a][m]);
                }
            }

        }
    }
    await fs.writeFile("data.json", JSON.stringify(data));
})();