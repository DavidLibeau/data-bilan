const fs = require('fs');
const pdf = require('pdf-parse');
 
let dataBuffer = fs.readFileSync('recueil-75-2018-391-recueil-des-actes-administratifs-special du 22 11 2018.pdf');
 
pdf(dataBuffer).then(function(data) {
 
    // number of pages
    console.log(data.numpages);
    // number of rendered pages
    console.log(data.numrender);
    // PDF info
    console.log(data.info);
    // PDF metadata
    console.log(data.metadata); 
    // PDF.js version
    // check https://mozilla.github.io/pdf.js/getting_started/
    console.log(data.version);
    // PDF text
    //console.log(data.text);
    var text = data.text.split("Sommaire\n")[1];
    text = text.split("2\n")[0];
    var sommaire = text.split(" - ");
    var arretes = [];
    for(var s in sommaire){
        sommaire[s] = sommaire[s].split("Page ")[0];
        sommaire[s] = sommaire[s].replace("\n", " ");
        sommaire[s] = sommaire[s].trim();
        if(sommaire[s].indexOf("Arrêté") != -1){
            arretes.push(sommaire[s]);
        }
    }
    console.log(sommaire);
    console.log(arretes);
    console.log(JSON.stringify(arretes));
        
});