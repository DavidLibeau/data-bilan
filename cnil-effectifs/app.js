import fetch from 'node-fetch';
import fs from 'fs'
import fse from 'fs-extra/esm'
import puppeteer from 'puppeteer';

const browser = await puppeteer.launch();
const page = await browser.newPage();

const links = await fse.readJson('data.json');

for (var l in links) {
  var link = links[l];
  console.log(link);
  await page.goto(link[0]);
  const resultsSelector = '.page-content';
  await page.waitForSelector(resultsSelector);
  const body = await page.evaluate(resultsSelector => {
    return [...document.querySelectorAll(resultsSelector)].map(anchor => {
      return anchor.textContent;
    })[0];
  }, resultsSelector);
  console.log(body);
  await fse.writeJson(link[0].split("cnil/id/")[1]+".json", [...link, body]);
}
await browser.close();