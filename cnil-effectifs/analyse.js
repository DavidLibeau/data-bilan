import * as dotenv from 'dotenv';
import fetch from 'node-fetch';
import fs from 'fs'
import fse from 'fs-extra/esm'
import {
  createHash
} from 'node:crypto'
import moment from 'moment';

const mois = ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"];

function sha256(content) {
  return createHash('sha256').update(content.replace(/  /g, " ").toLowerCase().replace("monsieur", "m.").replace("madame", "mme").replace("ç", "c") + process.env.SALT).digest('hex')
}

var manualDate = {
  "CNILTEXT000017651826": "23 novembre 2006",
  "CNILTEXT000017651991": "25 janvier 2007",
  "CNILTEXT000017652163": "13 juin 2006",
  "CNILTEXT000017652167": "25 avril 2007",
  "CNILTEXT000017652234": "14 septembre 2006",
  "CNILTEXT000017652243": "28 septembre 2006",
  "CNILTEXT000017653186": "9 novembre 2004",
};

var database = {};
var dateDetected = {};
var moments = {};
var momentsSorted = [];
var peopleDetected = {};
var peopleResult = {};
var countResult = {};
var intituleResult = {};
var intituleStats = {};
var plaintesResult = {};


fs.readdir('data', async function (err, files) {
  if (err)
    console.log(err);
  else {
    for (var f in files) {
      var file = files[f];
      var data = await fse.readJson('data/' + file);
      if (data[1].indexOf("habilit") != -1 && data[1].indexOf("rapport") == -1) {
        //console.log(file);
        var date = data[1].split(" du ")[1]?.split(/habilitant|portant/)[0];
        if (!date) {
          date = manualDate[file.replace(".json", "")];
        }
        date = date.trim();
        //console.log(date);
        var people = data[2].split("Décide")[1].split("Article 2")[0].match(/((M. )|(Mme )|(Monsieur )|(Madame )).{1,30},/g).map(m => sha256(m));
        dateDetected[date] = people;
        var intitules = data[2].indexOf("\n") != -1 && data[2].split("Décide")[1].split("Article 2")[0].match(/((M. )|(Mme )|(Monsieur )|(Madame )).*/g).map(m => m.split(",")[1].trim());
        if (intitules) {
          intituleResult[date] = intitules;
        }
        var plainteString = data[2].indexOf("\n") != -1 && data[2].split("Décide")[1].split("Article 2")[0].match(/plainte/g);
        if (plainteString) {
          plaintesResult[date] = plainteString.length;
        }
        //console.log(people.length);
        for (var p in people) {
          if (!peopleDetected[people[p]]) {
            peopleDetected[people[p]] = [];
          }
          peopleDetected[people[p]].push(date);
        }
        //console.log(people);
        //console.log(data[2].indexOf("pour une durée de cinq ans") != -1);
        database[date] = {
          plaintes:plaintesResult[date] ?? 0,
          people:people.length,
        };
      }
    }
    for (var date in dateDetected) {
      try {
        var moisNb = mois.indexOf(date.split(" ")[1]) + 1 || parseInt(date.split(" ")[1]);
        var jourNb = date.split(" ")[0];
        var dateFormatted = date.replace(date.split(" ")[1], moisNb < 10 ? "0" + moisNb : moisNb);
        dateFormatted = dateFormatted.replace(jourNb, jourNb < 10 ? "0" + jourNb : jourNb);
        dateFormatted = dateFormatted.replace(/ /g, "/");
        var parsedDate = moment.parseZone(dateFormatted, 'DD/MM/YYYY', 'fr', true);
        if (parsedDate && parsedDate.format("L") != 'Invalid date') {
          moments[moment(parsedDate).toISOString()] = date;
        } else {
          console.log("Error on date: " + date + " : " + dateFormatted);
        }
      } catch (e) {
        console.log("Error on date " + date + " : " + dateFormatted);
      }
    }
    momentsSorted = Object.keys(moments).sort();
    var lastPeople = [];
    for (var m in momentsSorted) {
      var momentSorted = momentsSorted[m];
      //console.log(momentSorted);
      //console.log(moments[momentSorted]);
      var peopleByMoment = dateDetected[moments[momentSorted]];
      var currentPeople = [];
      for (var p in peopleByMoment) {
        var people = peopleByMoment[p];
        if (!peopleResult[people]) {
          peopleResult[people] = {
            start: momentSorted,
            end: momentSorted
          };
        }
        currentPeople.push(people);
      }
      for (var p in lastPeople) {
        if (currentPeople.indexOf(lastPeople[p]) != -1) {
          peopleResult[lastPeople[p]].end = momentSorted;
        }
      }
      lastPeople = currentPeople;
    }
    //console.log(momentsSorted[momentsSorted.length-1]);
    //console.log(lastPeople);
    for (var p in lastPeople) {
      peopleResult[lastPeople[p]].end = "2023-01-01T00:00:00.000Z";
    }
    for (var m in momentsSorted) {
      var momentSorted = momentsSorted[m];
      countResult[momentSorted] = 0;
      for (var p in peopleResult) {
        var people = peopleResult[p];
        if (people.start == momentSorted || people.end == momentSorted || moment(momentSorted) >= moment(people.start) && moment(momentSorted) <= moment(people.end)) {
          countResult[momentSorted]++;
        }
      }
    }
    fs.readFile("opencnil-volumes-plaintes-depuis-1981-maj-07-2022.csv", "utf-8", function (err, data) {
      var plaintesByYear = {};
      var years = data.split("\r\n")[0].replace("Année ;", "").split(";");
      var plaintes = data.split("\r\n")[1].replace("Nombre de plaintes reçues ;", "").split(";");

      console.log(years);
      console.log(plaintes);

      for (var y in years) {
        plaintesByYear[years[y]] = plaintes[y];
      }

      for (var c in countResult) {
        console.log(c + ";" + countResult[c]);
      }
      console.log("--");
      for (var c in countResult) {
        console.log(c + ";" + ((plaintesByYear[parseInt(c.split("-")[0])]) / countResult[c]));
      }

      console.log("--");
      for (var date in intituleResult) {
        intituleStats[date] = {};
        for (var i in intituleResult[date]) {
          var intitule = intituleResult[date][i].toLowerCase().split(" ")[0].replace("ée", "é").replace("trice","teur").replace("cheffe","chef").replace("assistante","assistant").replace("adjointe","adjoint");
          if(intitule[0]=="m"){
            intitule = "juriste"; // un nom mal formaté
          }
          if(intitule == "chargé"){
            var intitule = intituleResult[date][i].toLowerCase().split(/ (au)|à|-|(violation)/)[0].trim().replace("ée", "é").replace("trice","teur").replace("cheffe","chef").replace("assistante","assistant").replace("adjointe","adjoint");
          }
          if (!intituleStats[date][intitule]) {
            intituleStats[date][intitule] = 0;
          }
          intituleStats[date][intitule]++;
        }
      }
      var keysIntitule = {};
      for (var date in intituleStats) {
        for(var key in intituleStats[date]){
          if(!keysIntitule[key]){
            keysIntitule[key] = 0;
          }
          keysIntitule[key]++;
        }
        console.log(date+" : "+Object.keys(intituleStats[date]).length);
      }
      console.log(keysIntitule);

      console.log("--");
      console.log("Juristes");
      for(var m in momentsSorted){
        var date = moments[momentsSorted[m]];
        
        if(intituleStats[date]){
          console.log(date+" : "+intituleStats[date].juriste+" / "+database[date].people+" = "+parseInt(intituleStats[date].juriste/database[date].people*100)+" %");
        }
      }
      console.log("--");
      console.log("Auditeurs/ingénieurs");
      for(var m in momentsSorted){
        var date = moments[momentsSorted[m]];
        
        if(intituleStats[date]){
          console.log(date+" : "+(intituleStats[date].auditeur+intituleStats[date].ingénieur)+" / "+database[date].people+" = "+parseInt((intituleStats[date].auditeur+intituleStats[date].ingénieur)/database[date].people*100)+" %");
        }
      }

      console.log("--");
      console.log("Plaintes string")
      for(var m in momentsSorted){
        var date = moments[momentsSorted[m]];
        if(database[date] && database[date].plaintes!=0){
          console.log(date+" : "+database[date].plaintes+" / "+database[date].people+" = "+parseInt(database[date].plaintes/database[date].people*100)+" %");
        }
      }

      fse.writeJson("peopleResult.json", peopleResult);
      fse.writeJson("countResult.json", countResult);
      fse.writeJson("intituleStats.json", intituleStats);
    });
  }
})